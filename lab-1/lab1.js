// ������� ����� Animal ��� ������������� �������� � ��������
class Animal {
    constructor(species, category, habitat) {
        this.species = species;
        this.category = category;
        this.habitat = habitat;
    }
}

// ������� ����� Employee ��� ������������� ����������� ��������
class Employee {
    constructor(name, position) {
        this.name = name;
        this.position = position;
    }

    feedAnimal(animal, food) {
        console.log(`${this.name} feeds ${animal.species} with ${food.name}.`);
    }
}

// ������� ����� Enclosure ��� ������������� �������� � ��������
class Enclosure {
    constructor(name, size, type) {
        this.name = name;
        this.size = size;
        this.type = type;
        this.animals = [];
    }

    addAnimal(animal) {
        this.animals.push(animal);
        console.log(`${animal.species} was added to the ${this.name} enclosure.`);
    }
}

// ������� ����� Food ��� ������������� ����� ��� ��������
class Food {
    constructor(name, category) {
        this.name = name;
        this.category = category;
    }
}

// ������� ����� Inventory ��� ����� ��������� ��������
class Inventory {
    constructor() {
        this.animals = [];
        this.employees = [];
    }

    addAnimal(animal) {
        this.animals.push(animal);
    }

    addEmployee(employee) {
        this.employees.push(employee);
    }

    display() {
        console.log("Inventory:");
        console.log("Animals:", this.animals.map(animal => animal.species));
        console.log("Employees:", this.employees.map(employee => employee.name));
    }
}

// ������� ���������� ������� ��� �������
const tiger = new Animal('Tiger', 'Mammal', 'Jungle');
const zookeeper = new Employee('Jane Doe', 'Zookeeper');
const tigerEnclosure = new Enclosure('Tiger Habitat', 'Large', 'Outdoor');
const meat = new Food('Meat', 'Carnivore');
const inventory = new Inventory();

tigerEnclosure.addAnimal(tiger);

zookeeper.feedAnimal(tiger, meat);

inventory.addAnimal(tiger); 
inventory.addEmployee(zookeeper);

inventory.display();
